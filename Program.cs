﻿using System;

namespace exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            //Declare variables
            var userNum1 = 0;

            //Ask user to enter a number
            Console.WriteLine("Please enter any number between 1 and 50");

            //Store the number entered as var userNum1
            userNum1 = int.Parse(Console.ReadLine());

            //Display to screen userNum1 multiplied by digits 1-12 on separate lines
            Console.WriteLine($"1 x {userNum1} = {userNum1*1}");
            Console.WriteLine($"2 x {userNum1} = {userNum1*2}");
            Console.WriteLine($"3 x {userNum1} = {userNum1*3}");
            Console.WriteLine($"4 x {userNum1} = {userNum1*4}");
            Console.WriteLine($"5 x {userNum1} = {userNum1*5}");
            Console.WriteLine($"6 x {userNum1} = {userNum1*6}");
            Console.WriteLine($"7 x {userNum1} = {userNum1*7}");
            Console.WriteLine($"8 x {userNum1} = {userNum1*8}");
            Console.WriteLine($"9 x {userNum1} = {userNum1*9}");
            Console.WriteLine($"10 x {userNum1} = {userNum1*10}");
            Console.WriteLine($"11 x {userNum1} = {userNum1*11}");
            Console.WriteLine($"12 x {userNum1} = {userNum1*12}");

            Console.ReadKey();

            
        }
    }
}
